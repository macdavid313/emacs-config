;;;; Font and Themes
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["black" "#d55e00" "#009e73" "#f8ec59" "#0072b2" "#cc79a7" "#56b4e9" "white"])
 '(custom-enabled-themes (quote (deeper-blue)))
 '(safe-local-variable-values (quote ((Syntax . ANSI-Common-Lisp) (Base . 10)))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:family "Fantasque Sans Mono" :foundry "PfEd" :slant normal :weight normal :height 120 :width normal)))))

;;; Elpa, the default package repository for emacs is fairly conservative, so
;;; we'll add the melpa and marbaraie repositories
(require 'package)
(setq package-archives '(("gnu" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
			 ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")
			 ("marmalade" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/marmalade/")
			 ("org" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/org/")))
(package-initialize) ;; you may already have this line

;;; The following 3 lines disable unnecessary GUI elements, in this case the
;;; menu bar, the tool bar and the scroll bar. If you wish, you can comment out
;;; the menu-bar and keep it, but eventually I recommend you disable it.
(if (fboundp 'menu-bar-mode) (menu-bar-mode -1))
(if (fboundp 'tool-bar-mode) (tool-bar-mode -1))
(if (fboundp 'scroll-bar-mode) (scroll-bar-mode -1))

;;; when `yes-or-no', y -> yes, n -> no
(fset 'yes-or-no-p 'y-or-n-p)

;;; Binds F7 to view recent files
(recentf-mode 1)
(global-set-key (kbd "<f7>") 'recentf-open-files)

;;; `disk' utility
;;; This unifies disk operations on a file.  Use it by binding it to a
;;; key.  Example setup in your ~/.emacs file:
(global-set-key (kbd "<f9>") 'disk)
(autoload 'disk "disk" "Save, revert, or find file." t)

;; Sometimes the mini-buffer becomes multi-line, and it can be a bit annoying as
;; you type in it. This makes it stay one line.
(setq resize-mini-windows nil)

;;; We don't need the Emacs splash screen. You can keep it on if you're into
;;; that sort of thing
(setq inhibit-splash-screen t)

;;; global-linum-mode adds line numbers to all open windows, In my opinion
;;; there is no reason not to have this enabled.
(global-linum-mode)

;;; Emacs comes with a built-in text based browser. Since I use the browse-url
;;; function most often to browse documentation, I've set it to eww, the Emacs
;;; Web Browser. It works well for that purpose. If you would prefer to use a
;;; graphical browser, you can change this line.
(setq browse-url-browser-function 'eww-browse-url)

;;quick access hacker news
(defun hn ()
  (interactive)
  (browse-url "http://news.ycombinator.com"))

;;quick access reddit
(defun reddit (reddit)
  "Opens the REDDIT in w3m-new-session"
  (interactive (list
                (read-string "Enter the reddit (default: lisp): " nil nil "lisp" nil)))
  (browse-url (format "http://m.reddit.com/r/%s" reddit))
  )

;; quick read <let over lambda> book
(defun lol (chapter)
  "Reading the book <Let Over Lambda> by chapter. If `chapter' is not specified, open the Table of Contentes instead."
  (interactive (list
                (read-string "Enter the chapter (default: Table of Contents): " nil nil "toc" nil)))
  (if (string= chapter "toc")
      (browse-url "http://www.letoverlambda.com/textmode.cl/guest/toc")
    (browse-url (format "http://www.letoverlambda.com/textmode.cl/guest/chap%s.html" chapter))))

;; quick access Common Lisp the Language 2nd Edition
(defun cltl2 ()
  (interactive)
  (eww-open-file "~/Documents/cltl2/clm/node1.html"))

;; quick access SBCL Manual
(defun sbcl-doc ()
  (interactive)
  (browse-url "http://www.sbcl.org/manual/index.html"))

;; quick access Allegro CL 10.0 Documentation
(defun acl-doc ()
  (interactive)
  (eww-open-file "~/Documents/Allegro CL/contents.htm"))

;; quick access LispWorks Documentation
;; looks ugly ...
(defun lispworks-doc ()
  (interactive)
  (eww-open-file "/home/david/Documents/LispWorks Documentation/intro.htm"))

;; quickdocs, a CL projects infomation site
(defun quickdocs (proj)
  (interactive
   (list (read-string "Enter the project's name (default: alexandria): " nil nil "alexandria" nil)))
  (browse-url (format "http://quickdocs.org/%s" proj)))

;; read CFFI Manual
(defun cffi-manual ()
  (interactive)
  (eww-open-file "~/Documents/cffi-manual.html"))

;; I need this very often
(defun wikipedia-search (search-term)
  "Search for SEARCH-TERM on wikipedia"
  (interactive
   (let ((term (if mark-active
                   (buffer-substring (region-beginning) (region-end))
                 (word-at-point))))
     (list
      (read-string
       (format "Wikipedia (%s):" term) nil nil term))))
  (browse-url
   (concat
    "http://en.m.wikipedia.org/w/index.php?search="
    search-term)))

;;; I prefer to make the default font slightly smaller.
(set-face-attribute 'default nil :height 120)

;; Show matching parentecies globaly.
(show-paren-mode 1)

;;; Enable company globally for all mode
;;(global-company-mode)

;;; Reduce the time after which the company auto completion popup opens
(setq company-idle-delay 0.2)

;;; Reduce the number of characters before company kicks in
(setq company-minimum-prefix-length 1)

;;; Use UTF-8 by default
(set-language-environment "UTF-8")

;;; Don't ring the bell. It saves some annoyance
(setq ring-bell-function 'ignore)

;;; If you're one of the heathens who prefers tabs over spaces, you should
;;; remove the following line. It makes indentation use spaces.
(setq-default indent-tabs-mode nil)

;;; Disabling Auto Save
;; (setq auto-save-default nil) ; not activated
;;; Preventing the Creation of Backup Files
;; (setq make-backup-files nil) ; not activated
;;; A simple backup setup. Makes sure I don't foo~ and #.foo files in
;;; directories with files you edit.
(setq backup-by-copying t ; don't clobber symlinks-
      backup-directory-alist '(("." . "~/.saves")) ; don't litter my fs tree
      auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
      delete-old-versions t
      kept-new-versions 6
      kept-old-versions 2
      version-control t) ; use versioned backups

;;; confirm-nonexistent-file-or-buffer is a variable defined in `files.el'.
;;; Its value is after-completion
;;;
;;; Documentation:
;;; Whether confirmation is requested before visiting a new file or buffer.
;;; If nil, confirmation is not requested.
;;; If the value is `after-completion', confirmation is only 
;;; requested if the user called `minibuffer-complete' right before
;;; `minibuffer-complete-and-exit'.
;;; Any other non-nil value means to request confirmation.
;;;
;;; This affects commands like `switch-to-buffer' and `find-file'.
;;;
;;; You can customize this variable.
;;; 
;;; This variable was introduced, or its default value was changed, in
;;; version 23.1 of Emacs.
(setq confirm-nonexistent-file-or-buffer nil)

;;; Set up emacs server. This allows you to run emacs in the background and
;;; connect to it with emacs client. It reduces startup time significantly. If
;;; the server is not running, it starts it.
(load "server")
(unless (server-running-p) (server-start))

;;; ido-mode, or Interactively DO mode, adds lots of improvements when working
;;; with buffers of files. You can read more about it at:
;;; https://www.emacswiki.org/emacs-test/InteractivelyDoThings
(require 'ido)
(ido-mode t)

;;; SMEX
(require 'smex) ; Not needed if you use package.el
;; Can be omitted.
;;This might cause a (minimal) delay when Smex is auto-initialized on its first run.
;;(smex-initialize) 
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; This is your old M-x.
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)
(defadvice smex (around space-inserts-hyphen activate compile)
  (let ((ido-cannot-complete-command
         `(lambda ()
            (interactive)
            (if (string= " " (this-command-keys))
                (insert ?-)
              (funcall ,ido-cannot-complete-command)))))
    ad-do-it))

;;; By default C-x o is bound to 'other window, but I find I use it much more
;;; ofther than open-line, which is bound to C-o, so I swap their definitions
(global-set-key (kbd "C-o") 'other-window)
(global-set-key (kbd "C-x o") 'open-line)

;;; M-0..3 are bound to 'digit-argument. To be used with C-u. I don't use them
;;; ofthen, so I prefer to rebind them to the window commands, since M-1 is
;;; easyer to type than C-x 1.
(global-set-key (kbd "M-1") 'delete-other-windows)
(global-set-key (kbd "M-2") 'split-window-vertically)
(global-set-key (kbd "M-3") 'split-window-horizontally)
(global-set-key (kbd "M-0") 'delete-window)

;;; Set the enter key to newline-and-indent which inserts a new line and then
;;; indents according to the major mode. This is very convenient.
(global-set-key (kbd "<RET>") 'newline-and-indent)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;; Lisp Customizations ;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -- common-lisp compatibility if not added earlier in your .emacs
(require 'cl)

;;; mainly for Allegro, they use .cl as surfix by default
(add-to-list 'auto-mode-alist '("\\.cl\\'" . lisp-mode))

;;; Slime Development Environment ---------------------------------
;; Setup load-path, autoloads and your lisp system
(add-to-list 'load-path "/Users/david/common-lisp/slime")
(require 'slime-autoloads)
;; Also setup the slime-fancy contrib
(setq slime-contribs '(slime-fancy slime-fuzzy slime-asdf slime-banner
                                   slime-xref-browser slime-highlight-edits slime-scratch
                                   slime-trace-dialog slime-mdot-fu))
;; Mutiple Lisps
(setq slime-lisp-implementations
      '((alisp ("/Applications/AllegroCL32.app/Contents/Resources/alisp"
                "-L" "~/quicklisp/setup.lisp"))
	(mlisp ("/Applications/AllegroCL32.app/Contents/Resources/mlisp"
                "-L" "~/quicklisp/setup.lisp"))
	(sbcl ("/usr/local/bin/sbcl"
               "--load" "/Users/david/quicklisp/setup.lisp"))
	(ccl ("/usr/local/bin/ccl"
              "--load" "~/quicklisp/setup.lisp"))
	(ecl ("/usr/local/bin/ecl" "-load" "~/quicklisp/setup.lisp"))
	(clisp ("/usr/local/bin/clisp" "--load" "~/quicklisp/setup.lisp"))
	(abcl ("/usr/local/bin/abcl" "--load" "~/quicklisp/setup.lisp"))))

;; set default Lisp to SBCL
(setf slime-default-lisp 'sbcl)

;; set default encoding to UTF-8-UNIX
(setq slime-net-coding-system 'utf-8-unix)

(defun lisp-hook-fn ()
  (interactive)
  ;; Start slime mode (slime-mode)
  ;; Some useful key-bindings
  (local-set-key [tab] 'slime-complete-symbol)
  (local-set-key (kbd "M-q") 'slime-reindent-defun)
  ;; We set the indent function. common-lisp-indent-function
  ;; will indent our code the right way
  (set (make-local-variable lisp-indent-function) 'common-lisp-indent-function)
  ;; We tell slime to not load failed compiled code
  (setq slime-load-failed-fasl 'never))
;; Finally we tell lisp-mode to run our function on startup
(add-hook 'lisp-mode-hook 'lisp-hook-fn)

;;; ac-slime ------------------------------------------------------
;;(require 'ac-slime)
;;(add-hook 'slime-mode-hook 'set-up-slime-ac)
;;(add-hook 'slime-repl-mode-hook 'set-up-slime-ac)
;;(eval-after-load "auto-complete"
  ;;'(add-to-list 'ac-modes 'slime-repl-mode))

;; slime-company --------------------------------------------------
(slime-setup '(slime-company))
(define-key company-active-map (kbd "\C-n") 'company-select-next)
(define-key company-active-map (kbd "\C-p") 'company-select-previous)
(define-key company-active-map (kbd "\C-d") 'company-show-doc-buffer)
(define-key company-active-map (kbd "M-.") 'company-show-location)

;; CLHS ----------------------------------------------------------
(setq common-lisp-hyperspec-root "file:///Users/david/.emacs.d/HyperSpec-7-0/HyperSpec/")

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;; OCaml Customizations ;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; -- Tuareg mode -----------------------------------------
;; Add Tuareg to your search path
(add-to-list
 'load-path
 ;; Change the path below to be wherever you've put your tuareg installation.
 (expand-file-name "/Users/david/.emacs.d/elpa/tuareg-20151108.1834/"))
(require 'tuareg)
(setq auto-mode-alist 
      (append '(("\\.ml[ily]?$" . tuareg-mode))
	      auto-mode-alist))

;; -- Tweaks for OS X -------------------------------------
;; Tweak for problem on OS X where Emacs.app doesn't run the right
;; init scripts when invoking a sub-shell
(cond
 ((eq window-system 'ns) ; macosx
  ;; Invoke login shells, so that .profile or .bash_profile is read
  (setq shell-command-switch "-lc")))

;; -- opam and utop setup --------------------------------
;; Setup environment variables using opam
(dolist
    (var (car (read-from-string
	       (shell-command-to-string "opam config env --sexp"))))
  (setenv (car var) (cadr var)))
;; Update the emacs path
(setq exec-path (split-string (getenv "PATH") path-separator))
;; Update the emacs load path
(push (concat (getenv "OCAML_TOPLEVEL_PATH")
	      "/../../share/emacs/site-lisp") load-path)
;; Automatically load utop.el
(autoload 'utop "utop" "Toplevel for OCaml" t)
(autoload 'utop-minor-mode "utop" "Minor mode for utop" t)
(add-hook 'tuareg-mode-hook 'utop-minor-mode)

;; -- merlin setup ---------------------------------------

(setq opam-share (substring (shell-command-to-string "opam config var share") 0 -1))
(add-to-list 'load-path (concat opam-share "/emacs/site-lisp"))
(require 'merlin)

;; Enable Merlin for ML buffers
(add-hook 'tuareg-mode-hook 'merlin-mode)

;; So you can do it on a mac, where `C-<up>` and `C-<down>` are used
;; by spaces.
(define-key merlin-mode-map
  (kbd "C-c <up>") 'merlin-type-enclosing-go-up)
(define-key merlin-mode-map
  (kbd "C-c <down>") 'merlin-type-enclosing-go-down)
(set-face-background 'merlin-type-face "#88FF44")

;; -- enable auto-complete -------------------------------
;; Not required, but useful along with merlin-mode
(require 'auto-complete)
(add-hook 'tuareg-mode-hook 'auto-complete-mode)

;;; Font and Themes
(when (eq system-type 'darwin)

  ;; default Latin font (e.g. Consolas)
  (set-face-attribute 'default nil :family "Fantasque Sans Mono")

  ;; default font size (point * 10)
  ;;
  ;; WARNING!  Depending on the default font,
  ;; if the size is not supported very well, the frame will be clipped
  ;; so that the beginning of the buffer may not be visible correctly. 
  (set-face-attribute 'default nil :height 160)

  ;; use specific font for Korean charset.
  ;; if you want to use different font size for specific charset,
  ;; add :size POINT-SIZE in the font-spec.
  ;;(set-fontset-font t 'hangul (font-spec :name "NanumGothicCoding"))

  ;; you may want to add different for other charset in this way.
  )
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#ded6c5" "#f71010" "#028902" "#ef8300" "#0000ff" "#a020f0" "#358d8d" "#262626"])
 '(ansi-term-color-vector
   [unspecified "#424242" "#EF9A9A" "#C5E1A5" "#FFEE58" "#64B5F6" "#E1BEE7" "#80DEEA" "#E0E0E0"])
 '(beacon-color "#ec4780")
 '(custom-enabled-themes (quote (deeper-blue)))
 '(custom-safe-themes
   (quote
    ("d9a0d14596e3d0bdb81f052fa9b99741dcd239af402d42e35f80822e05557cb2" "e8586a76a96fd322ccb644ca0c3a1e4f4ca071ccfdb0f19bef90c4040d5d3841" "0788bfa0a0d0471984de6d367bb2358c49b25e393344d2a531e779b6cec260c5" "304c03c9cfcd368b4ab0832357788cd48513fe1bd89b9e531dd47886a83405a1" "a0feb1322de9e26a4d209d1cfa236deaf64662bb604fa513cca6a057ddf0ef64" "8c75217782ccea7e9f3ad2dae831487a5fb636d042263d0a0e0438d551da3224" "2a5be663818e1e23fd2175cc8dac8a2015dcde6b2e07536712451b14658bbf68" "51277c9add74612c7624a276e1ee3c7d89b2f38b1609eed6759965f9d4254369" "ab04c00a7e48ad784b52f34aa6bfa1e80d0c3fcacc50e1189af3651013eb0d58" "38150ddfbe25a3c94cf8ec2fddde0b1f292ef631fed373938a42544e58a5f80f" "b7b2cd8c45e18e28a14145573e84320795f5385895132a646ff779a141bbda7e" "a1289424bbc0e9f9877aa2c9a03c7dfd2835ea51d8781a0bf9e2415101f70a7e" "790e74b900c074ac8f64fa0b610ad05bcfece9be44e8f5340d2d94c1e47538de" "55d31108a7dc4a268a1432cd60a7558824223684afecefa6fae327212c40f8d3" "ea489f6710a3da0738e7dbdfc124df06a4e3ae82f191ce66c2af3e0a15e99b90" "726dd9a188747664fbbff1cd9ab3c29a3f690a7b861f6e6a1c64462b64b306de" "04dd0236a367865e591927a3810f178e8d33c372ad5bfef48b5ce90d4b476481" "977513781c8dd86f4f0a04dbf518df5ba496da42b71173368b305478703eea42" "b869a1353d39ab81b19eb79de40ff3e7bb6eaad705e61f7e4dbdcb183f08c5a6" "a0bbe4dc3513cbd049eb95f79c467b6f19dc42979fec27a0481bb6980bd8d405" "86a731bda96ed5ed69980b4cbafe45614ec3c288da3b773e4585101e7ece40d2" "cadc97db0173a0d0bfc40473cab4da462af0ba8d60befd0a4879b582bcbc092d" "fbcdb6b7890d0ec1708fa21ab08eb0cc16a8b7611bb6517b722eba3891dfc9dd" "b61c55259c639a54628f91452b060b99c550a1269eb947e372321b806b68f114" "f9d34593e9dd14b2d798494609aa0fddca618145a5d4b8a1819283bc5b7a2bfd" "5e3fc08bcadce4c6785fc49be686a4a82a356db569f55d411258984e952f194a" "70340909b0f7e75b91e66a02aa3ad61f3106071a1a4e717d5cdabd8087b47ec4" "1db337246ebc9c083be0d728f8d20913a0f46edc0a00277746ba411c149d7fe5" "532769a638787d1196bc22c885e9b85269c3fc650fdecfc45135bb618127034c" "70f5a47eb08fe7a4ccb88e2550d377ce085fedce81cf30c56e3077f95a2909f2" "53d91dd074ca40087e5ec47842086a5cc5b0d019fdc591089e8ebb1e56b08d4f" "6998bd3671091820a6930b52aab30b776faea41449b4246fdce14079b3e7d125" "e87a2bd5abc8448f8676365692e908b709b93f2d3869c42a4371223aab7d9cf8" "7153b82e50b6f7452b4519097f880d968a6eaf6f6ef38cc45a144958e553fbc6" "5a0eee1070a4fc64268f008a4c7abfda32d912118e080e18c3c865ef864d1bea" "c3e6b52caa77cb09c049d3c973798bc64b5c43cc437d449eacf35b3e776bf85c" "68d36308fc6e7395f7e6355f92c1dd9029c7a672cbecf8048e2933a053cf27e6" "bcc6775934c9adf5f3bd1f428326ce0dcd34d743a92df48c128e6438b815b44f" default)))
 '(diary-entry-marker (quote font-lock-variable-name-face))
 '(emms-mode-line-icon-image-cache
   (quote
    (image :type xpm :ascent center :data "/* XPM */
static char *note[] = {
/* width height num_colors chars_per_pixel */
\"    10   11        2            1\",
/* colors */
\". c #358d8d\",
\"# c None s None\",
/* pixels */
\"###...####\",
\"###.#...##\",
\"###.###...\",
\"###.#####.\",
\"###.#####.\",
\"#...#####.\",
\"....#####.\",
\"#..######.\",
\"#######...\",
\"######....\",
\"#######..#\" };")))
 '(evil-emacs-state-cursor (quote ("#E57373" bar)) t)
 '(evil-insert-state-cursor (quote ("#E57373" hbar)) t)
 '(evil-normal-state-cursor (quote ("#FFEE58" box)) t)
 '(evil-visual-state-cursor (quote ("#C5E1A5" box)) t)
 '(fci-rule-color "#f6f0e1")
 '(gnus-logo-colors (quote ("#0d7b72" "#adadad")) t)
 '(gnus-mode-line-image-cache
   (quote
    (image :type xpm :ascent center :data "/* XPM */
static char *gnus-pointer[] = {
/* width height num_colors chars_per_pixel */
\"    18    13        2            1\",
/* colors */
\". c #358d8d\",
\"# c None s None\",
/* pixels */
\"##################\",
\"######..##..######\",
\"#####........#####\",
\"#.##.##..##...####\",
\"#...####.###...##.\",
\"#..###.######.....\",
\"#####.########...#\",
\"###########.######\",
\"####.###.#..######\",
\"######..###.######\",
\"###....####.######\",
\"###..######.######\",
\"###########.######\" };")) t)
 '(highlight-symbol-colors
   (quote
    ("#FFEE58" "#C5E1A5" "#80DEEA" "#64B5F6" "#E1BEE7" "#FFCC80")))
 '(highlight-symbol-foreground-color "#E0E0E0")
 '(highlight-tail-colors (quote (("#ec4780" . 0) ("#424242" . 100))))
 '(nrepl-message-colors
   (quote
    ("#CC9393" "#DFAF8F" "#F0DFAF" "#7F9F7F" "#BFEBBF" "#93E0E3" "#94BFF3" "#DC8CC3")))
 '(pos-tip-background-color "#3a3a3a")
 '(pos-tip-foreground-color "#9E9E9E")
 '(tabbar-background-color "#353535")
 '(vc-annotate-background "#f6f0e1")
 '(vc-annotate-color-map
   (quote
    ((20 . "#e43838")
     (40 . "#f71010")
     (60 . "#ab9c3a")
     (80 . "#9ca30b")
     (100 . "#ef8300")
     (120 . "#958323")
     (140 . "#1c9e28")
     (160 . "#3cb368")
     (180 . "#028902")
     (200 . "#008b45")
     (220 . "#077707")
     (240 . "#259ea2")
     (260 . "#358d8d")
     (280 . "#0eaeae")
     (300 . "#2c53ca")
     (320 . "#0000ff")
     (340 . "#0505cc")
     (360 . "#a020f0"))))
 '(vc-annotate-very-old-color "#a020f0"))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

;;;;; SPARQL-mode
(add-to-list 'load-path "~/.emacs.d/elpa/sparql-mode-20151104.914/")
(autoload 'sparql-mode "sparql-mode.el"
    "Major mode for editing SPARQL files" t)
(add-to-list 'auto-mode-alist '("\\.sparql$" . sparql-mode))
(add-to-list 'auto-mode-alist '("\\.rq$" . sparql-mode))
(add-hook 'sparql-mode-hook 'company-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Rust Dev ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Load rust-mode when you open `.rs` files
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))

;;; racer
(add-hook 'rust-mode-hook #'racer-mode)
(setq racer-rust-src-path "/usr/local/src/rust/src/")
(setq racer-cmd "~/.cargo/bin/racer")

(require 'company-racer)

(with-eval-after-load 'company
  (add-to-list 'company-backends 'company-racer))

;;;; flymake-rust
(require 'flymake-rust)
(add-hook 'rust-mode-hook 'flymake-rust-load)

;; Setting up configurations when you load rust-mode
(add-hook 'rust-mode-hook         
          '(lambda ()

             ;; Hook in racer with eldoc to provide documentation
             (racer-turn-on-eldoc)

             ;; Use flycheck-rust in rust-mode
             (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)

             ;; Use company-racer in rust mode
             (set (make-local-variable 'company-backends) '(company-racer))

             ;; Key binding to jump to method definition
             (local-set-key (kbd "M-.") #'racer-find-definition)

             ;; Key binding to auto complete and indent
                  (local-set-key (kbd "TAB")
                  #'racer-complete-or-indent)))

;;; Launchctl
(require 'launchctl)
(add-to-list 'auto-mode-alist '("\\.plist$" . nxml-mode))

;;; default files will be open on starting
(find-file "/Users/david/.emacs")
(eshell)

;;; Org Mode
;; The following lines are always needed.
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))
(add-hook 'org-mode-hook 'turn-on-font-lock) ; not needed when global-font-lock-mode is on
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)
(add-hook 'org-mode-hook (lambda () (setq truncate-lines nil)))
(setq-default org-use-sub-superscripts nil)
(setq org-export-with-sub-superscripts nil)

;;; λ
(global-set-key "\C-cL" "λ")

;;; Octave
(setq auto-mode-alist (cons '("\\.m$" . octave-mode) auto-mode-alist))
(add-hook 'octave-mode-hook
           (lambda ()
             (abbrev-mode 1)
             (auto-fill-mode 1)
             (if (eq window-system ’x)
                 (font-lock-mode 1))))

;;; ace-jump-mode
(require 'ace-jump-mode)
(define-key global-map (kbd "C-c SPC") 'ace-jump-mode)
(require 'ace-jump-buffer)
(require 'ace-pinyin)

;;; web-mode
(require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.phtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tpl\\.php\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.[agj]sp\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.as[cp]x\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.djhtml\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(setq web-mode-markup-indent-offset 2)
(setq web-mode-css-indent-offset 2)

;;; scriba-mode
(load-file "~/.emacs.d/scriba.el")
(require 'scriba)

;;; Carp mode
(load-file "~/.emacs.d/carp-mode.el")
(load-file "~/.emacs.d/inf-carp-mode.el")
(put 'downcase-region 'disabled nil)
